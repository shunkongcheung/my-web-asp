﻿var InteractiveBoard = function () {

    // chess
    var initialRowValue = "Invalid Row";
    var playerFromRowName = "Fromrow"; var playerFromColName = "Fromcol";
    var playerToRowName = "Torow"; var playerToColName = "Tocol";

    var compFromRowName = "CompFromRow"; var compFromColName = "CompFromCol";
    var compToRowName = "CompToRow"; var compToColName = "CompToCol";
    
    var moveChessId;
    var toPositionId;
    var clickAfterMove;

    var JumpHeight = 15;
    var curJump = 0;

    // button
    var submitBtnName = "btnSubmit";

    // board 
    var boardName = "board";
    var boardOriginWidth, boardOriginHeight;


    function interactiveBoardInit() {

        // set value to initial value
        document.getElementById(playerFromRowName).value = initialRowValue;
        document.getElementById(playerToRowName).value = initialRowValue;

        if (document.getElementById(compFromRowName).value != -1) {
            moveChessId = document.getElementById(compFromRowName).value + '-' + document.getElementById(compFromColName).value;
            toPositionId = document.getElementById(compToRowName).value + '-' + document.getElementById(compToColName).value;
            clickAfterMove = false;
        }
        else {
            moveChessId = null;
            toPositionId = null;
        }
        

        // get board size
        var boardElem = document.getElementById(boardName);
        boardOriginWidth = boardElem.getBoundingClientRect().width;
        boardOriginHeight = boardElem.getBoundingClientRect().height;

        // reset board size
        boardResize();
    }

    function boardResize() {

        // reest board size
        var documentRoot = (document.compatMode === "CSS1Compat") ? document.documentElement : document.body;
        var browserheight = (documentRoot.clientHeight - 65);
        var browserwidth = documentRoot.clientWidth * 0.9;

        // presume board size is okay
        var boardElem = document.getElementById(boardName);
        boardElem.style.width = boardOriginWidth + 'px';
        boardElem.style.height = boardOriginHeight + 'px';

        // change board size accordingly
        if (browserwidth < boardElem.getBoundingClientRect().width) {
            boardElem.style.width = browserwidth + 'px';
            boardElem.style.height = browserwidth * boardOriginHeight / boardOriginWidth + 'px';
        }
        if (browserheight < boardElem.getBoundingClientRect().height) {
            boardElem.style.height = browserheight + 'px';
            boardElem.style.width = browserheight * boardOriginWidth / boardOriginHeight + 'px';
        }

        // change chess accordingly
        var chesswidth = boardElem.getBoundingClientRect().width / 9;
        var chessheight = boardElem.getBoundingClientRect().height / 10;

        for (var i = 0; i < 10; i++)
            for (var j = 0; j < 9; j++) {

                var id = i + '-' + j;
                var element = document.getElementById(id);

                element.style.left = boardElem.getBoundingClientRect().left - document.body.getBoundingClientRect().left + chesswidth * j + 'px';
                element.style.top = boardElem.getBoundingClientRect().top - document.body.getBoundingClientRect().top + chessheight * i + 'px';

                element.style.width = chesswidth + 'px';
                element.style.height = chessheight + 'px';
            }

        // make sure the board is at the central of the browser
        window.scrollTo(0, 65);
    }

    function boardImageClick(id) {

        // check if from position is set
        var fromrow = document.getElementById(playerFromRowName).value;

        // if board initial position is not set yet
        var clickCoord = id.split("-");

        if (fromrow == initialRowValue) {
            // set jump for from chess
            document.getElementById(playerFromRowName).value = clickCoord[0];
            document.getElementById(playerFromColName).value = clickCoord[1];
            moveChessId = id;
            curJump = 0;
        }
        else {
            // set movement
            document.getElementById(playerToRowName).value = clickCoord[0];
            document.getElementById(playerToColName).value = clickCoord[1];
            toPositionId = id;
        }
    }

    function movement() {
        jumping();
        moving();
    }

    function jumping() {

        // jump only if there is chess set and no destination
        if ( moveChessId == null || toPositionId != null)
            return;

        // jump or restart
        var movechess = document.getElementById(moveChessId);
        if (curJump < JumpHeight) {
            movechess.style.top = movechess.getBoundingClientRect().top - document.body.getBoundingClientRect().top - 1 + 'px';
            curJump++;
        }
        else {
            movechess.style.top = movechess.getBoundingClientRect().top - document.body.getBoundingClientRect().top + curJump + 'px';
            curJump = 0;
            clickAfterMove = true;
        }
    }

    function moving() {
        // move only if there is chess set and destination
        if ( moveChessId == null ||toPositionId == null)
            return;

        // get from and to image
        var destination = document.getElementById(toPositionId);
        var movechess = document.getElementById(moveChessId);

        // get position of two image
        var fromPixels = [movechess.getBoundingClientRect().left - document.body.getBoundingClientRect().left,
                        movechess.getBoundingClientRect().top - document.body.getBoundingClientRect().top];

        var toPixels = [destination.getBoundingClientRect().left - document.body.getBoundingClientRect().left,
                            destination.getBoundingClientRect().top - document.body.getBoundingClientRect().top];

        // close up distance 
        var diff = [(toPixels[0] - fromPixels[0]) / 10,
        (toPixels[1] - fromPixels[1]) / 10];

        // move the elemnt
        movechess.style.left = fromPixels[0] + diff[0] + 'px';
        movechess.style.top = fromPixels[1] + diff[1] + 'px';

        // moving is finished
        if (diff[0] < 1 && diff[0] > -1 && 
            diff[1] < 1 && diff[1] > -1) {

            // remove to image
            destination.parentNode.removeChild(destination);

            // rename from image to toImage's name
            movechess.id = toPositionId;

            // create a new image for fromImage position
            var img = document.createElement("img");
            img.setAttribute('src', '/Image/blank.png');
            img.setAttribute('class', "chess");
            img.setAttribute('id', moveChessId);
            img.setAttribute('onclick', 'myInteractiveBoard.boardImageClick(id)');
            document.body.appendChild(img);

            moveChessId = null;
            toPositionId = null;

            boardResize();

            if (clickAfterMove) {
                document.getElementById(submitBtnName).click();
            }

        }


    }
    
    // public functions
    return {
        interactiveBoardInit: interactiveBoardInit,
        boardResize: boardResize,
        boardImageClick: boardImageClick,
        boardMovement: movement
    }
}
