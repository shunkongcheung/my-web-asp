﻿using System;
using MyWeb.GameChess;
using System.Collections.Generic;

namespace MyWeb.ChessAnalyse
{
    public class AnalyzerNode
    {
        // information about the choice
        public Choice MyChoice { get; }

        // this board
        public Board MyBoard { get; }

        // previous move to get to this board
        public Move UpLayerMove { get; }

        // tree structure
        public AnalyzerNode Parent { get; set; }
        private List<AnalyzerNode> Children;
        private int chosenChild;
        
        
        public AnalyzerNode(Choice choice, Board board, Move upLayerMove)
        {
            // information about the node
            MyChoice = choice;
            MyBoard = new Board(board);
            UpLayerMove = upLayerMove;

            if(MyChoice.move != null && MyChoice.move.FromCoord.Row != -1)
                MyBoard.GetMoveWithChess(MyChoice.move);

            // the choice. Initialization of child pointer will be done later
            Parent = null;
            Children = new List<AnalyzerNode>();
            chosenChild = -1;
        }

        public AnalyzerNode GetIndexedChilden(int index)
        {
            return Children[index];
        }

        public int GetChildCount() { return Children.Count; }

        public AnalyzerNode GetChosenChild()
        {
            return Children[chosenChild];
        }

        public void SetChildren(List<AnalyzerNode> list)
        {
            // given a list of node. Put them all into children
            // list is using the same AnalyzerNode structure.
            // use first child pointer to next

            Children = list;
           
            // create expect board
            bool isValidMove = (MyChoice.move.FromCoord.Row != -1);
            if (isValidMove)
                MyBoard.MoveBoard(MyChoice.move);
            

            // find the chosen children, also set the parent
            foreach (AnalyzerNode child in Children)
            {
                child.Parent = this;
                if (isValidMove && IsSameBoard(child.MyBoard, MyBoard))
                    chosenChild = Children.IndexOf(child);
            }
            
            // get my board back
            if (isValidMove)
                MyBoard.RevertBoard(MyChoice.move);
            
        }

        private bool IsSameBoard(Board b1, Board b2)
        {
            for (int i = 0; i < Board.BoardHeight; i++)
                for (int j = 0; j < Board.BoardWidth; j++)
                    if (b1.GetChessName(i, j) != b2.GetChessName(i, j) ||
                        b1.GetChessPlayer(i, j) != b2.GetChessPlayer(i, j))
                        return false;
            return true;
        }
    }
}
