﻿using System;
using MyWeb.GameChess;
using System.Collections.Generic;

namespace MyWeb.ChessAnalyse
{
    public class AnalyzerTreeBuilder
    {
        // this is compulsary information
        public int TreeDepth { get; }

        // other information that may be useful when analyzing
        public Chess.PlayerIndex StartSide { get; }
        public int SelfLimit { get; }


        // for buliding tree
        private List<AnalyzerNode>[] LevelNodes;


        public AnalyzerTreeBuilder(int treeDepth, Chess.PlayerIndex startSide, int limit)
        {
            TreeDepth = treeDepth;
            StartSide = startSide;
            SelfLimit = limit;

            LevelNodes = new List<AnalyzerNode>[TreeDepth + 1];
            for (int i = 0; i < TreeDepth + 1; i++)
                LevelNodes[i] = new List<AnalyzerNode>();
        }

        public AnalyzerTreeBuilder(int treeDepth)
        {
            TreeDepth = treeDepth;
            StartSide = Chess.PlayerIndex.Empt;
            SelfLimit = -1;

            LevelNodes = new List<AnalyzerNode>[TreeDepth + 1];
            for (int i = 0; i < TreeDepth+1; i++)
                LevelNodes[i] = new List<AnalyzerNode>();
        }

        public AnalyzerNode GetTreeRoot()
        {
            return LevelNodes[TreeDepth][0];
        }

        public void InsertNode(int curDepth, Choice choice, Board board, Move upLayerMove)
        {
            if (curDepth > TreeDepth || curDepth < 0)
            {
                throw new IndexOutOfRangeException();
            }

            // create new node
            AnalyzerNode insert = new AnalyzerNode(choice, board, upLayerMove);

            // insert to respective level
            LevelNodes[curDepth].Add(insert);

            // if there are something below my level. They are now my children
            int lowlevel = curDepth - 1;
            if (lowlevel >= 0)
            {
                if (LevelNodes[lowlevel].Count > 0)
                {
                    insert.SetChildren(LevelNodes[lowlevel]);
                    LevelNodes[lowlevel] = new List<AnalyzerNode>();
                }
            }
        }

        public AnalyzerNode GetTreeRoot(Choice choice, Board board)
        {
            AnalyzerNode insert = new AnalyzerNode(choice, board, null);
            insert.SetChildren(LevelNodes[0]);
            return insert;
        }

        public void InsertNode(AnalyzerNode node)
        {
            LevelNodes[0].Add(node);
        }
    }

}
