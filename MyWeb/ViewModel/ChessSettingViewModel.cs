﻿using MyWeb.GameChess;
using System;
using System.Collections.Generic;


namespace MyWeb.ViewModel
{
    public class ChessSettingViewModel
    {
        public class DiffLevel
        {
           public String LevelName { get; set; }
           public int Value { get; set; }
        }

        public class Starter
        {
            public String StarterName { get; set; }
            public int Value { get; set; }
        }

        public class Side
        {
            public String SideName { get; set; }
            public Chess.PlayerIndex Value { get; set; }
        }

        public IEnumerable<DiffLevel> DifficultyChoice = new List<DiffLevel>()
            {
                new DiffLevel{LevelName="Difficult", Value=(int)Choose.Difficulty.Difficult},
                new DiffLevel{LevelName="Normal", Value=(int)Choose.Difficulty.Medium},
                new DiffLevel{LevelName="Easy", Value=(int)Choose.Difficulty.Easy}
            };

        public IEnumerable<Starter> StarterChoice = new List<Starter>()
            {
                new Starter{StarterName="Client", Value=0},
                new Starter{StarterName="Computer", Value=1}
            };

        public IEnumerable<Side> SideChoice = new List<Side>()
            {
                new Side{SideName="Black", Value=Chess.PlayerIndex.Bot},
                new Side{SideName="Red", Value= Chess.PlayerIndex.Top}
            };        
    }
}