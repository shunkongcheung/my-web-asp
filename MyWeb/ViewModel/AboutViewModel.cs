﻿using MyWeb.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MyWeb.ViewModel
{
    public class AboutViewModel
    {
        public PersonalInfo PersonalInfo { get; set; }
        public List<WorkExp> WorkExperiences { get; set; }
    }
}