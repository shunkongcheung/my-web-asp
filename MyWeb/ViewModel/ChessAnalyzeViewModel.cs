﻿using MyWeb.ChessAnalyse;
using MyWeb.GameChess;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MyWeb.ViewModel
{
    public class ChessAnalyzeViewModel
    {
        public Board MyBoard { get; set; }
        public AnalyzerNode MyAnalyzerNode { get; set; }
    }
}