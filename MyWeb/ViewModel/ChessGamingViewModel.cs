﻿using MyWeb.Models;
using MyWeb.GameChess;
using System;
using System.Collections.Generic;

namespace MyWeb.ViewModel
{
    public class ChessGamingViewModel
    {
        // server to client
        public BoardImages BoardImages { get; set; }
        public Board Board { get; set; }

        // client to server
        public Move Move { get; set; }
        
        public string Message { get; set; }
        
        public Move MyMove { get; set; }
        
        // settings
        public class DiffLevel
        {
            public String LevelName { get; set; }
            public int Value { get; set; }
        }

        public class Starter
        {
            public String StarterName { get; set; }
            public int Value { get; set; }
        }

        public class Side
        {
            public String SideName { get; set; }
            public Chess.PlayerIndex Value { get; set; }
        }

        public IEnumerable<DiffLevel> DifficultyChoice = new List<DiffLevel>()
            {
                new DiffLevel{LevelName="Difficult", Value=(int)Choose.Difficulty.Difficult},
                new DiffLevel{LevelName="Normal", Value=(int)Choose.Difficulty.Medium},
                new DiffLevel{LevelName="Easy", Value=(int)Choose.Difficulty.Easy}
            };
        

        public IEnumerable<Side> SideChoice = new List<Side>()
            {
                new Side{SideName="Bottom", Value=Chess.PlayerIndex.Bot},
                new Side{SideName="Top", Value= Chess.PlayerIndex.Top}
            };

        public ChessGamingViewModel()
        {
            BoardImages = new BoardImages();

            Move = new Move();

            Message = "";
            MyMove = new Move(-1,-1,-1,-1);
        }
    }
}