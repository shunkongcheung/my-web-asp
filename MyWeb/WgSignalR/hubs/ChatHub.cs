﻿using Microsoft.AspNet.SignalR;
using Microsoft.AspNet.SignalR.Hubs;

namespace MyWeb.Signalr.hubs
{
  //  [HubName("chat")]
    public class ChatHub: Hub
    {
        public void Broadcast(string msg)
        {
            Clients.All.addNewMessageToPage(msg);
        }
    }
}