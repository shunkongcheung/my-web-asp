﻿using System;

namespace MyWeb.Models
{
    public class PersonalInfo
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string BirthDate { get; set; }
        public string EmailAddr { get; set; }
        public string MailAddr { get; set; }
    }
}