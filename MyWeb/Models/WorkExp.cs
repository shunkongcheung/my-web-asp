﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MyWeb.Models
{
    public class WorkExp
    {
        public string CompanyName { get; set; }
        public string Department { get; set; }
        public string Location { get; set; }

        public string Title { get; set; }

        public string StartDate { get; set; }
        public string EndDate { get; set; }

        public List<string> Descriptions { get; set; }
    }
}