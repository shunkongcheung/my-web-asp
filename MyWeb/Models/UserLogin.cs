﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace MyWeb.Models
{
    public class UserLogin
    {
        [Display(Name ="Email Address")]
        [Required(AllowEmptyStrings =false , ErrorMessage ="Email Address Required!")]
        public string EmailAddr { get; set; }

        [Display(Name ="Password")]
        [DataType(DataType.Password)]
        [Required(AllowEmptyStrings =false, ErrorMessage ="Password Required!")]
        public string Password { get; set; }
        
        [Display(Name ="Remember Me")]
        public bool RememberMe { get; set; }
    }
}