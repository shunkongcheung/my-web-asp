﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MyWeb.Models
{
    public class BoardImages
    {
        public string BoardFile { get; set; }

        public string BlackGeneralFile { get; set; }
        public string BlackChariotFile { get; set; }
        public string BlackHorseFile { get; set; }
        public string BlackArtillaryFile { get; set; }
        public string BlackEscortFile { get; set; }
        public string BlackJumboFile { get; set; }
        public string BlackSoldierFile { get; set; }

        public string RedGeneralFile { get; set; }
        public string RedChariotFile { get; set; }
        public string RedHorseFile { get; set; }
        public string RedArtillaryFile { get; set; }
        public string RedEscortFile { get; set; }
        public string RedJumboFile { get; set; }
        public string RedSoldierFile { get; set; }

        public string BlankFile { get; set; }

        public BoardImages()
        {
            BoardFile = "~/Image/board.png";

            BlackGeneralFile = "~/Image/blackgeneral.png";
            BlackChariotFile = "~/Image/blackchariot.png";
            BlackHorseFile = "~/Image/blackhorse.png";
            BlackArtillaryFile = "~/Image/blackartillary.png";
            BlackEscortFile = "~/Image/blackescort.png";
            BlackJumboFile = "~/Image/blackjumbo.png";
            BlackSoldierFile = "~/Image/blacksoldier.png";

            RedGeneralFile = "~/Image/redgeneral.png";
            RedChariotFile = "~/Image/redchariot.png";
            RedHorseFile = "~/Image/redhorse.png";
            RedArtillaryFile = "~/Image/redartillary.png";
            RedEscortFile = "~/Image/redescort.png";
            RedJumboFile = "~/Image/redjumbo.png";
            RedSoldierFile = "~/Image/redsoldier.png";

            BlankFile = "~/Image/blank.png";
        }
    }
}