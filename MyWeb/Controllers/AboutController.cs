﻿using System;
using System.Web.Mvc;
using System.Xml;

using MyWeb.Models;
using MyWeb.ViewModel;
using System.Collections.Generic;
using System.Data;

namespace MyWeb.Controllers
{
    public class AboutController : Controller
    {
        // GET: About/Index
        public ActionResult Index()
        {
            var aboutViewModel = new AboutViewModel
            {
                PersonalInfo = GetPersonalInfo("~/data/personalinfo.xml"),
                WorkExperiences = GetExperiences("~/data/experiences.xml")
            };

            return View(aboutViewModel);
        }


        private PersonalInfo GetPersonalInfo(string filepath)
        {
            string firstname = "Shun Kong";
            string lastname = "Cheung";
            string birthdate = "9th March 1995";
            string email = "shunkongcheung@gmail.com";
            string mail = "flat 6, 21/f, block k, Fanling Centre, N.T. Hong Kong";
            
            try
            {
                // read file as string
                var fileContents = System.IO.File.ReadAllText(Server.MapPath(filepath));

                // put it as xml file
                XmlDocument xmlPerInfo = new XmlDocument();
                xmlPerInfo.LoadXml(fileContents);

                // get information
                XmlNode rootPerInfo = xmlPerInfo.SelectSingleNode("/personalInfo");
                mail = rootPerInfo["email"].InnerText;
                email = rootPerInfo["mail"].InnerText;
            }
            catch (Exception e) { }


            //create personal information object
            var personalInfo = new PersonalInfo
            {
                FirstName = firstname,
                LastName = lastname,
                BirthDate = birthdate,
                EmailAddr = email,
                MailAddr = mail
            };

            return personalInfo;
        }

        private List<WorkExp> GetExperiences(string filepath)
        {

            List<WorkExp> workexps = new List<WorkExp>();
            
            try
            {
                // read file as string
                var fileContents = System.IO.File.ReadAllText(Server.MapPath(filepath));

                // read string as xml file
                XmlDocument xmlExp = new XmlDocument();
                xmlExp.LoadXml(fileContents);
      
                // extract information
                XmlNodeList rootPerInfo = xmlExp.SelectNodes("/experiences/experience");
                foreach (XmlNode experience in rootPerInfo)
                {
                    // descrptions
                    List<string> descriptions = new List<string>();
                    XmlNodeList descripts = experience.SelectNodes("descriptions/description");
                    foreach (XmlNode descript in descripts)
                    {
                        descriptions.Add(descript.InnerText);
                    }
                    
                    // create work experience object
                    workexps.Add(new WorkExp
                    {
                        CompanyName = experience["companyname"].InnerText,
                        Department = experience["department"].InnerText,
                        Location = experience["location"].InnerText,

                        Title = experience["title"].InnerText,

                        StartDate = experience["startdate"].InnerText,
                        EndDate = experience["enddate"].InnerText,

                        Descriptions = descriptions
                    });
                }
            } catch (Exception e) { }
            
            return workexps;
        }

    }

}