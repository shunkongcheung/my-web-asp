﻿using MyWeb.GameChess;
using MyWeb.ViewModel;
using MyWeb.WgStatic;
using System;
using System.IO;
using System.Web.Mvc;

namespace MyWeb.Controllers
{
    public class ChessController : Controller
    {
        // GET: Chess
        [Authorize]
        public ActionResult Index()
        {
            // ip
            string myid = User.Identity.Name;
            //User.Identity.Name;            
            // get game
            ChessContent content = MyStaticObj.GetContent(myid);

            // create game if not exist
            if (content == null)
            {
                content = GetChessContent(Choose.Difficulty.Medium, Chess.PlayerIndex.Bot, myid);
                MyStaticObj.InsertContent(myid, content);
            }

            // create new game if the game was done
            if (content.GetGameBoard().Getwinner() != Chess.PlayerIndex.Empt)
                MyStaticObj.InsertContent(myid, GetChessContent(Choose.Difficulty.Medium, Chess.PlayerIndex.Bot, myid));

            // display game board
            return View(
                "Game",
                new ChessGamingViewModel
                {
                    Board = content.GetGameBoard(),
                    Message = "Welcome",

                });
        }

        [Authorize]
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult Submit(ChessGamingViewModel collection)
        {
            // get move
            Move playerMove = null;
            try
            {
                playerMove = new Move(collection.Move.FromCoord.Row, collection.Move.FromCoord.Col,
                 collection.Move.ToCoord.Row, collection.Move.ToCoord.Col);
            }
            catch (Exception e)
            {
                return View("Error");
            }

            // get object from Static object
            string myid =  User.Identity.Name;
            ChessContent content = MyStaticObj.GetContent(myid);

            // check if finished first
            bool isFinished = (content.GetGameBoard().Getwinner() != Chess.PlayerIndex.Empt);
            if (isFinished)
            {
                String fileContents = System.IO.File.ReadAllText(Server.MapPath("~/data/startboard.xml"));
                content = GetChessContent(Choose.Difficulty.Difficult, Chess.PlayerIndex.Bot, myid);
                MyStaticObj.InsertContent(myid, content);
            }

            // check if it is a valid move and also move the board
            bool isValid = content.MoveGameBoard(myid, playerMove);

            // check if finish
            isFinished = (content.GetGameBoard().Getwinner() != Chess.PlayerIndex.Empt);

            // computer move if valid and not finished
            StringWriter message = new StringWriter();
            Move compMove = new Move(-1, -1, -1, -1);
            Board board = content.GetGameBoard();

            if (!isValid)
                message.Write("(" + playerMove.FromCoord.Row + ", " + playerMove.FromCoord.Col
                    + ":" + content.GetGameBoard().GetChessName(playerMove.FromCoord.Row, playerMove.FromCoord.Col)
                    + ") to (" + playerMove.ToCoord.Row + ", " + playerMove.ToCoord.Col + ") is an invalid move.");

            if (isValid && !isFinished)
            {
                // make choice and move
                Choice choice = content.CompMakeMove();
                compMove = choice.move;
                message.Write("Computer moved from (" + choice.move.FromCoord.Row + ", " + choice.move.FromCoord.Col
                     + ":" + board.GetChessName(choice.move.FromCoord.Row, choice.move.FromCoord.Col) + ")" +
                   " to (" + choice.move.ToCoord.Row + ", " + choice.move.ToCoord.Col
                    + ":" + board.GetChessName(choice.move.ToCoord.Row, choice.move.ToCoord.Col) + ")"
                   );

                //check if computer won the game
                isFinished = (content.GetGameBoard().Getwinner() != Chess.PlayerIndex.Empt);
            }

            // create message
            if (isFinished)
                message.Write(content.GetGameBoard().Getwinner() + " player has won the game");

            // display game board
            return View(
                "Game",
                new ChessGamingViewModel
                {
                    Board = board,
                    Message = message.ToString(),
                    MyMove = compMove
                });
        }

        [Authorize]
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult Setting(ChessSettingViewModel chessSetting, FormCollection form)
        {

            ChessContent content = MyStaticObj.GetContent( User.Identity.Name);

            // create object
            Choose.Difficulty diff = (Choose.Difficulty)int.Parse((form["DifficultyChoice"].ToString()));
            Chess.PlayerIndex playerSide = (form["SideChoice"].ToString() == Chess.PlayerIndex.Top.ToString()) ? Chess.PlayerIndex.Top : Chess.PlayerIndex.Bot;

            string myid =  User.Identity.Name;
            content.SetDiff(diff);
            content.SetPlayerSide(playerSide, myid);
            content.SetPlayerSide((playerSide == Chess.PlayerIndex.Top) ? Chess.PlayerIndex.Bot : Chess.PlayerIndex.Top, null);

            // display game board
            return View(
                "Game",
                new ChessGamingViewModel
                {
                    Board = content.GetGameBoard(),
                    Message = "Set to " + playerSide + " and " + diff,
                    MyMove = new Move(-1, -1, -1, -1)
                });
        }

        [Authorize]
        public ActionResult Regret()
        {
            ChessContent content = MyStaticObj.GetContent( User.Identity.Name);
            content.RegretMove();
            content.RegretMove();

            return View(
                "Game",
                 new ChessGamingViewModel
                 {
                     Board = content.GetGameBoard(),
                     Message = "Hi Loser!"
                 });
        }

        [Authorize]
        public ActionResult Restart()
        {
            string myid =  User.Identity.Name;
            ChessContent content = GetChessContent(Choose.Difficulty.Medium, Chess.PlayerIndex.Bot, myid);
            MyStaticObj.InsertContent( User.Identity.Name, content);

            return View(
                "Game",
                 new ChessGamingViewModel
                 {
                     Board = content.GetGameBoard(),
                     Message = "Let's do it again then! I am not afraid!"
                 });
        }

        [Authorize]
        public ActionResult Analyze(string command)
        {
            ChessContent content = MyStaticObj.GetContent( User.Identity.Name);

            switch (command)
            {
                case "up": content.GoUpTree(); break;
                case "dive": content.GoDiveTree(); break;
                default:
                    int index;
                    if (int.TryParse(command, out index)) content.GoDiveTree(index);
                    break;
            }

            return View(
                 new ChessAnalyzeViewModel
                 {
                     MyAnalyzerNode = content.GetAnalyzerTree(),
                     MyBoard = content.GetAnalyzerTree().MyBoard
                 }
                 );
        }

        [NonAction]
        private ChessContent GetChessContent(Choose.Difficulty diff, Chess.PlayerIndex player, string id)
        {
            // display setting
            String fileContents = System.IO.File.ReadAllText(Server.MapPath("~/data/startboard.xml"));
            ChessContent content = new ChessContent(fileContents);
            content.SetDiff(diff);
            content.SetPlayerSide(player, id);

            return content;
        }

    }
}