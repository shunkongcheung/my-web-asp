﻿using MyWeb.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Text;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;

namespace MyWeb.Controllers
{
    public class UserController : Controller
    {
        // GET: User
        public ActionResult Index()
        {
            return View();
        }

        // registration (Action)
        [HttpGet]
        public ActionResult Registration()
        {
            return View(new User());
        }

        // registration (Post)
        [HttpPost]
        public ActionResult Registration([Bind(Exclude = "isEmailVerified, ActicationCode")]User user)
        {

            bool status = false;
            string msg = "";
            StreamWriter logwriter = new StreamWriter(Server.MapPath("~/data/log.txt"), true);

            try
            {
                // model validation
                if (ModelState.IsValid)
                {
                    logwriter.WriteLine("Model State is valid, check if email exist");

                    // email address check if exist
                    if (IsEmailExist(user.EmailAddr))
                    {
                        logwriter.WriteLine("Email alread exist");
                        ModelState.AddModelError("EmailExist", "Email alread exist!");
                    }
                    else
                    {
                        logwriter.WriteLine("Email did not exist. create object");

                        // generate activation code
                        user.ActicationCode = Guid.NewGuid();

                        // set this email as not activated
                        user.isEmailVerified = false;

                        // password hashing
                        user.Password = GenHashedPassword(user.Password);
                        user.ConfirmPassword = GenHashedPassword(user.ConfirmPassword);

                        logwriter.WriteLine("Object created.. try to save object");

                        // save data to db
                        using (WgDataBaseEntities dc = new WgDataBaseEntities())
                        {
                            dc.Users.Add(user);
                            dc.SaveChanges();
                        }

                        logwriter.WriteLine("Object saved. send email confirmation");

                        // send email to user
                        SendActivationCodeToEmail(user.ActicationCode.ToString(), user.EmailAddr);

                        logwriter.WriteLine("Email sent");

                        // output message
                        msg = "Registration successfully done. Please activate your account by responding to email sent to : " + user.EmailAddr;
                        status = true;
                    }
                }
                else
                {
                    msg = "Invalid request";
                }
            }
            catch (Exception e)
            {
                logwriter.WriteLine("ERROR: " + e.GetBaseException().ToString());
            }

            logwriter.Close();

            // return view
            ViewBag.Status = status;
            ViewBag.Message = msg;
            return View(user);
        }

        // verify user email
        [HttpGet]
        public ActionResult VerifyAccount(string id)
        {
            using (WgDataBaseEntities dc = new WgDataBaseEntities())
            {
                dc.Configuration.ValidateOnSaveEnabled = false;

                var account = dc.Users.Where(a => a.ActicationCode == new Guid(id)).FirstOrDefault();
                if (account != null)
                {
                    account.isEmailVerified = true;
                    dc.SaveChanges();
                    ViewBag.Status = true;
                }
                else
                {
                    ViewBag.Message = "Invalid Request!";
                }
            }

            return View();
        }


        // login (action)
        [HttpGet]
        public ActionResult Login()
        {
            return View();
        }

        // login (post)
        [HttpPost]
        public ActionResult Login(UserLogin userLogin, string redirect)
        {
            // get account
            using (WgDataBaseEntities dc = new WgDataBaseEntities())
            {
                var account = dc.Users.Where(a => a.EmailAddr == userLogin.EmailAddr).FirstOrDefault();
                if (account == null)
                {
                    ViewBag.Message = "Invalid Account.";
                }
                else if (string.Compare(GenHashedPassword(userLogin.Password), account.Password) != 0)
                {
                    ViewBag.Message = "Invalid Password.";
                }
                else if(!account.isEmailVerified)
                {
                    ViewBag.Message = "Account has not been activated!";
                }
                else
                {
                    // generate ticket
                    int timeout = userLogin.RememberMe ? (60 * 24 * 30) : 20; // 1 month or other
                    var ticket = new FormsAuthenticationTicket(userLogin.EmailAddr, userLogin.RememberMe, timeout);
                    string encrypted = FormsAuthentication.Encrypt(ticket);

                    // generate coookeies
                    var cookie = new HttpCookie(FormsAuthentication.FormsCookieName, encrypted)
                    {
                        Expires = DateTime.Now.AddMinutes(timeout),
                        HttpOnly = true
                    };

                    // give this cookie to client
                    Response.Cookies.Add(cookie);

                    // redirect to page after login
                    if (Url.IsLocalUrl(redirect))
                    {
                        return Redirect(redirect);
                    }
                    else
                    {
                        return RedirectToAction("Index", "Home");
                    }
                }
            }
            return View();
        }

        // logout
        [Authorize]
        [HttpPost]
        public ActionResult Logout()
        {
            FormsAuthentication.SignOut();
            return RedirectToAction("Index", "Action");
        }


        // verify email
        [NonAction]
        public bool IsEmailExist(string emailAddr)
        {
            using (WgDataBaseEntities dc = new WgDataBaseEntities())
            {
                var v = dc.Users.Where(a => a.EmailAddr == emailAddr).FirstOrDefault();
                return v != null;
            }
        }

        // generate encrypted password
        [NonAction]
        public string GenHashedPassword(string value)
        {
            return Convert.ToBase64String(
                System.Security.Cryptography.SHA256.Create().ComputeHash(Encoding.UTF8.GetBytes(value))
                );
        }

        // verifiy email link
        [NonAction]
        public void SendActivationCodeToEmail(string activationCode, string emailAddr)
        {
            var verifyUrl = "/user/VerifyAccount/" + activationCode;
            var link = Request.Url.AbsoluteUri.Replace(Request.Url.PathAndQuery, verifyUrl);

            // create email object
            var fromEmail = new MailAddress("noreply.skcheung@gmail.com");
            var toEmail = new MailAddress(emailAddr);
            var EmailPassword = "winghanveryfat";

            // create email
            string subject = "Welcome. Your account is created!";
            string body = "<br/>we are excited to tell you that your account is succssefully created " +
                            "Please verify the account with below link" +
                           "<a href='" + link + "'>" + link + "</a>";

            // create smtp object
            var smtp = new SmtpClient
            {
                Host = "smtp.gmail.com",
                Port = 587,
                EnableSsl = true,
                DeliveryMethod = SmtpDeliveryMethod.Network,
                UseDefaultCredentials = false,
                Credentials = new NetworkCredential(fromEmail.Address, EmailPassword)
            };

            // send email
            using (var message = new MailMessage(fromEmail, toEmail)
            {
                Subject = subject,
                Body = body,
                IsBodyHtml = true
            })
                smtp.Send(message);
        }
    }
}