﻿using MyWeb.GameChess;

namespace MyWeb.WgStatic
{
    public static class MyStaticObj
    {
        private static MyCache MyGameContents { get; set; }

        public static void Start()
        {
            MyGameContents = new MyCache(100);
        }
        
        public static ChessContent GetContent(string id)
        {
            // TODO: currently there is only cache. But may also implement database
            return (ChessContent)MyGameContents.GetItem(id);
        }

        public static void InsertContent(string id, ChessContent content)
        {
            MyGameContents.InsertItem(id, content);
        }
    }
}