﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MyWeb.WgStatic
{
    public class MyCache
    {

        // counter
        private int Capacity { get; }
        private int ElementCount { get; set; }

        // my data
        private SortedDictionary<string, Object> MyCacheSet { get; set; }

        public MyCache(int capacity)
        {
            if (capacity <= 0) throw new ArgumentException("capacity must be positive");

            Capacity = capacity;
            ElementCount = 0;
            MyCacheSet = new SortedDictionary<string, Object>();
        }

        public void InsertItem(string id, Object data)
        {
            // if element already exist
            if (MyCacheSet.ContainsKey(id))
            {
                MyCacheSet.Remove(id);
                MyCacheSet.Add(id, data);
            }
            else
            {
                // increment database count
                if (ElementCount == Capacity)
                    MyCacheSet.Remove(MyCacheSet.ElementAt(0).Key);
                else
                    ElementCount++;

                // insert item
                MyCacheSet.Add(id, data);
            }

           
        }

        public Object GetItem(string id)
        {
            Object item = null;
            MyCacheSet.TryGetValue(id, out item);

            return item;
        }

        public SortedDictionary<string, Object> EmptyCache()
        {
            ElementCount = 0;

            SortedDictionary<string, Object> temp = MyCacheSet;
            MyCacheSet = new SortedDictionary<string, object>();

            return temp;
        }
    }
}