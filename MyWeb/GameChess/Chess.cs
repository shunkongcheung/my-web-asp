﻿
namespace MyWeb.GameChess
{
    public class Chess
    {
        public enum RoleIndex
        {
            GENERAL = 'G',
            CHARIOT = 'C',
            HORSE = 'H',
            ARTILLERY = 'A',
            ESCORT = 'E',
            JUMBO = 'J',
            SOLDIER = 'S',
            NOTHING = 'K'
        }
        public enum PlayerIndex
        {
            Top = 'T',
            Bot = 'B',
            Empt = 'p'
        }
        
        // helper class
        public RoleIndex Role { get; set; }
        public PlayerIndex Side { get; set; }

        public Chess()
        {
            Role = RoleIndex.NOTHING;
            Side = PlayerIndex.Empt;
        }

        public Chess(RoleIndex r, PlayerIndex s)
        {
            Role = r;
            Side = s;
        }

        public Chess(Chess src)
        {
            CopyChess(src);
        }

        public void CopyChess(Chess src)
        {
            Role = src.Role;
            Side = src.Side;
        }

        public static bool IsEqual(Chess c1, Chess c2)
        {
            return c1.Role == c2.Role && c1.Side == c2.Side;
        }
    }
}