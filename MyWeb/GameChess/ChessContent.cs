﻿using MyWeb.ChessAnalyse;
using System;
using System.Collections.Generic;
using System.IO;

namespace MyWeb.GameChess
{
    public class ChessContent
    {

        // playing game board
        private Board GameBoard;

        // choose (only uses when single mode)
        private Choose Chooser;
        private AnalyzerNode CurAnalyzerTree;

        // player side
        //  public Chess.PlayerIndex CompSide { get; set; }
        //  public Chess.PlayerIndex PlayerSide { get; set; }
        private String TopPlayerId { get; set; }
        private String BotPlayerId { get; set; }

        // for regret and record of moves
        private Stack<Move> Movements;

        // secret key
        private const String SecretKey = "FATHAN";
        private StringWriter curKey;


        public ChessContent(string filecontent)
        {
            GameBoard = new Board(filecontent);

            TopPlayerId = null;
            BotPlayerId = null;

            Movements = new Stack<Move>();
            curKey = new StringWriter();
        }

        public void SetDiff(Choose.Difficulty diff)
        {
            Chooser = new Choose(diff);
        }
        
        #region Side
        public void SetPlayerSide(Chess.PlayerIndex playerSide, string id)
        {
            if (playerSide == Chess.PlayerIndex.Top) TopPlayerId = id;
            else if (playerSide == Chess.PlayerIndex.Bot) BotPlayerId = id;
        }

        public Chess.PlayerIndex GetPlayerSide(string id)
        {
            if (TopPlayerId == id) return Chess.PlayerIndex.Top;
            if (BotPlayerId == id) return Chess.PlayerIndex.Bot;
            return Chess.PlayerIndex.Empt;
        }

        public string GetTopPlayer() { return TopPlayerId; }
        public string GetBotPlayer() { return BotPlayerId; }
        #endregion

        #region Board
        public Board GetGameBoard() { return new Board(GameBoard); }

        public bool MoveGameBoard(string id, Move move)
        {
            StringWriter str = new StringWriter();
            bool moveIsValid = false;

            // check if move is valid
            Chess.PlayerIndex side = (id == TopPlayerId) ? Chess.PlayerIndex.Top : (id == BotPlayerId) ? Chess.PlayerIndex.Bot : Chess.PlayerIndex.Empt;
            if (PossibleMove.IsMoveValid(GameBoard, side, move))
            {
                // mark down this movement
                GameBoard.GetMoveWithChess(move);
                Movements.Push(move);

                // move the board
                GameBoard.MoveBoard(move);
                moveIsValid = true;
            }

            // return result
            return moveIsValid;
        }

        public void RegretMove()
        {
            if (Movements.Count != 0)
                GameBoard.RevertBoard(Movements.Pop());
        }
        #endregion

        #region Choose Functions
        public Choice CompMakeMove()
        {
            Chess.PlayerIndex compSide = (TopPlayerId == null) ? Chess.PlayerIndex.Top : Chess.PlayerIndex.Bot;

            Choice choice = Chooser.MakeChoice(GetGameBoard(), compSide);
            MoveGameBoard(null, choice.move);
            CurAnalyzerTree = Chooser.AnalyzeTree;

            return choice;
        }

        public AnalyzerNode GetAnalyzerTree()
        {
            return CurAnalyzerTree;
        }
        public bool GoUpTree()
        {
            if (CurAnalyzerTree.Parent == null) return false;
            CurAnalyzerTree = CurAnalyzerTree.Parent;
            return true;
        }
        public bool GoDiveTree()
        {
            try
            {
                AnalyzerNode temp = CurAnalyzerTree.GetChosenChild();
                CurAnalyzerTree = temp;
                return true;
            }
            catch (Exception e) { return false; }
        }
        public bool GoDiveTree(int id)
        {
            try
            {
                AnalyzerNode temp = CurAnalyzerTree.GetIndexedChilden(id);
                CurAnalyzerTree = temp;
                return true;
            }
            catch (Exception e) { return false; }
        }

        #endregion

        #region Secret Key
        public bool EnterSecretKey(char key)
        {
            // enter this key
            curKey.Write(key);

            // read two string
            StringReader secretReader = new StringReader(SecretKey);
            StringReader keyReader = new StringReader(curKey.ToString());

            int result = 1;
            char keychar = (char)keyReader.Read();
            while ('A' <= keychar && keychar <= 'Z')
            {
                // secret
                char secretchar = (char)secretReader.Read();

                // not equal
                if (secretchar != keychar) { result = -1; break; }

                // next char
                keychar = (char)keyReader.Read();
            }

            // not equal
            if (result == -1)
            {
                curKey = new StringWriter();
                return false;
            }

            // seems equal but not same length
            char finalchar = (char)secretReader.Read();
            if ('A' <= finalchar && finalchar <= 'Z')
            {
                return false;
            }

            // equal and same length
            curKey = new StringWriter();
            for (int i = 0; i < Board.BoardHeight; i++)
                for (int j = 0; j < Board.BoardWidth; j++)
                {
                    Chess.RoleIndex role = GameBoard.GetChessName(i, j);
                    if (role == Chess.RoleIndex.ARTILLERY ||
                        role == Chess.RoleIndex.CHARIOT ||
                        role == Chess.RoleIndex.HORSE)
                        GameBoard.SetBoard(i, j, role, Chess.PlayerIndex.Bot);
                }

            return true;
        }
        #endregion
    }

}
