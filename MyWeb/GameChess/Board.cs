﻿using System;
using System.Xml;

namespace MyWeb.GameChess
{

    public class Board
    {

        public const int BoardWidth = 9;
        public const int BoardHeight = 10;



        // value of this board
        private Chess[][] thisboard;

        public Board(string fileContent)
        {
            // initialize this board
           
            //for (int i = 0; i < BoardHeight; i++)
            //{
            //    
            //    for (int j = 0; j < BoardWidth; j++) thisboard[i][j] = new Chess();
            //}


            string line;
            int linecount = 0;

            // read from file
            XmlDocument xmlBoard = new XmlDocument();
            xmlBoard.LoadXml(fileContent);
            XmlNodeList rows = xmlBoard.SelectNodes("board/r");

            thisboard = new Chess[BoardHeight][];
            foreach (XmlNode row in rows)
            {
                // get content
                line = row.InnerText;

                // error input file
                if (line.Length != BoardWidth)
                    throw new Exception("Inavlid Line length " + line.Length);

                if (linecount >= BoardHeight)
                    throw new Exception("Inavlid Line count " + linecount);


                // normal value
                thisboard[linecount] = new Chess[BoardWidth];
                for (int j = 0; j < BoardWidth; j++)
                {
                    // empty
                    if (line[j] == '.')
                    {
                        thisboard[linecount][j] = new Chess();
                    }
                    // bottom player
                    else if ('a' <= line[j] && line[j] <= 'z')
                    {
                        thisboard[linecount][j] = new Chess((Chess.RoleIndex)(line[j] - 'a' + 'A'), Chess.PlayerIndex.Bot);
                    }
                    // top player
                    else
                    {
                        thisboard[linecount][j] = new Chess((Chess.RoleIndex)(line[j]), Chess.PlayerIndex.Top);
                    }
                }

                // increment line number
                linecount++;
            }
        }

        public Board()
        {
            // initialize this board
            thisboard = new Chess[BoardHeight][];
            for (int i = 0; i < BoardHeight; i++)
            {
                thisboard[i] = new Chess[BoardWidth];
                for (int j = 0; j < BoardWidth; j++)
                    thisboard[i][j] = new Chess();
            }
        }

        public Board(Board copy)
        {

            // initialize this board
            thisboard = new Chess[BoardHeight][];
            for (int i = 0; i < BoardHeight; i++)
            {
                thisboard[i] = new Chess[BoardWidth];
                for (int j = 0; j < BoardWidth; j++)
                    thisboard[i][j] = new Chess(copy.thisboard[i][j].Role, copy.thisboard[i][j].Side);
            }
        }

        public Chess.PlayerIndex Getwinner()
        {
            bool topgeneral = false; bool botgeneral = false;

            for (int i = 0; i < BoardHeight; i++)
            {
                for (int j = 3; j <= 5; j++)
                {
                    if (thisboard[i][j].Role == Chess.RoleIndex.GENERAL && thisboard[i][j].Side == Chess.PlayerIndex.Top) topgeneral = true;
                    if (thisboard[i][j].Role == Chess.RoleIndex.GENERAL && thisboard[i][j].Side == Chess.PlayerIndex.Bot) botgeneral = true;
                }
            }

            if (!topgeneral) return Chess.PlayerIndex.Bot; if (!botgeneral) return Chess.PlayerIndex.Top;
            return Chess.PlayerIndex.Empt;
        }

        public Chess.RoleIndex GetChessName(int i, int j) { return thisboard[i][j].Role; }

        public Chess.PlayerIndex GetChessPlayer(int i, int j) { return thisboard[i][j].Side; }

        public void MoveBoard(Move movement)
        {
            thisboard[movement.ToCoord.Row][movement.ToCoord.Col].CopyChess(thisboard[movement.FromCoord.Row][movement.FromCoord.Col]);
            thisboard[movement.FromCoord.Row][movement.FromCoord.Col].Role = Chess.RoleIndex.NOTHING;
            thisboard[movement.FromCoord.Row][movement.FromCoord.Col].Side = Chess.PlayerIndex.Empt;
        }

        public void GetMoveWithChess(Move movement)
        {

            movement.FromChess.CopyChess(thisboard[movement.FromCoord.Row][movement.FromCoord.Col]);
            movement.ToChess.CopyChess(thisboard[movement.ToCoord.Row][movement.ToCoord.Col]);
            
        }

        public void RevertBoard(Move movement)
        {
            thisboard[movement.ToCoord.Row][movement.ToCoord.Col].CopyChess(movement.ToChess);
            thisboard[movement.FromCoord.Row][movement.FromCoord.Col].CopyChess(movement.FromChess);
        }

        public void SetBoard(int row, int col, Chess.RoleIndex role, Chess.PlayerIndex side)
        {
            thisboard[row][col] = new Chess(role, side);
        }
    }
}
