﻿//#define isAnalyze

using MyWeb.ChessAnalyse;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace MyWeb.GameChess
{
    public class Choose
    {
        public enum Difficulty
        {
            Difficult,
            Medium,
            Easy
        }

        // result through optimize testing
        private const int defaultMaxCount = 80000;
        private const int difficultMaxCount = 100000;

        private const int defaultLimit = 5;
        private const int difficultLimit = 10;
        
        private const int easyDepth = 4;
        private const int defaultDepth = 5;
        private const int diffDepth = 5;

        // should be the number provided by constant above
        private const int numThread = 32;
        
        // the metrics that is used
        private int MaxCount = defaultMaxCount;
        private int StartDepth = defaultDepth;
        private int LimitOnSelfLevel = defaultLimit;


        public AnalyzerNode AnalyzeTree;


        public Choose(Difficulty diff)
        {
            switch (diff)
            {
                case Difficulty.Easy: StartDepth = easyDepth; MaxCount = defaultMaxCount; LimitOnSelfLevel = defaultLimit; break;
                case Difficulty.Medium: StartDepth = defaultDepth; MaxCount = difficultMaxCount; LimitOnSelfLevel = defaultLimit; break;
                case Difficulty.Difficult: StartDepth = diffDepth; MaxCount = difficultMaxCount; LimitOnSelfLevel = difficultLimit; break;
            }
        }

        public Choice MakeChoice(Board board, Chess.PlayerIndex side)
        {
            // calculate expected size
            PossibleMove movelist = new PossibleMove();
            movelist.GetMoves(board, side);

            // calculate each thread handle how many move. Take the upper bound
            int interval = movelist.total / numThread;
            if (interval * numThread < movelist.total)
                interval += 1;

            // dynamic create choices
            Choice[] results = new Choice[numThread];
            AnalyzerNode[] analyzers = new AnalyzerNode[numThread];
            for (int i = 0; i < numThread; i++)
            {
                results[i] = new Choice(side);
                analyzers[i] = new AnalyzerNode(results[i], board, null);
            }

            // start choosing
            Thread[] threads = new Thread[numThread];
            for (int i = 0; i < numThread; i++)
            {
                threads[i] = new Thread(() => ChooseForOneInterval(results, analyzers, interval, board, side, movelist, StartDepth, MaxCount, LimitOnSelfLevel))
                {
                    Name = i.ToString()
                };
                threads[i].Start();
            }
            // wait for all thread to finish
            for (int i = 0; i < numThread; i++)
                threads[i].Join();

            // calculate best result
            Choice result = GetBestChoice(results, numThread, side);

            // random choice
             result = ChooseRandomly(result, results, numThread, side);

            // create the final tree
#if isAnalyze
            AnalyzerTreeBuilder finalBuilder = new AnalyzerTreeBuilder(1, side, LimitOnSelfLevel);
            for (int i = 0; i < numThread; i++)
            {
                if (analyzers[i] != null)
                    for (int j = 0; j < analyzers[i].GetChildCount(); j++)
                        finalBuilder.InsertNode(analyzers[i].GetIndexedChilden(j));
            }

            AnalyzeTree = finalBuilder.GetTreeRoot(result, board);
#endif



            // return value
            return result;
        }

        private static Choice GetBestChoice(Choice[] choices, int length, Chess.PlayerIndex side)
        {
            // choosing the best among results
            Choice result = new Choice(side);

            // no best result yet
            int bestIndex = 0;

            // loop through all results
            for (int i = 0; i < length; i++)
            {
                // compare result with current best result
                int compare = Choice.IsBetterChoice(choices[i], result, side);
                if (compare == 1)
                {
                    result = choices[i];
                    bestIndex = i;
                }
            }

            return choices[bestIndex];
        }


        private static Choice ChooseRandomly(Choice bestchoice, Choice[] results, int length, Chess.PlayerIndex side)
        {
            // count best choice
            int bestCount = 0;
            for (int i = 0; i < length; i++)
            {
                if (Choice.IsBetterChoice(results[i], bestchoice, side) == 0) bestCount++;
            }

            // Randomness
            Random rnd = new Random();

            // choose one random best choice
            int randBest = rnd.Next(bestCount) + 1;
            for (int i = 0; i < length; i++)
            {
                // if it is the best choice
                if (Choice.IsBetterChoice(results[i], bestchoice, side) == 0) { randBest--; }

                // chose this one
                if (randBest == 0) { bestchoice = new Choice(results[i]); break; }
            }
            return bestchoice;
        }

        private static void ChooseForOneInterval(Choice[] results, AnalyzerNode[] analyzers,
            int interval, Board inboard, Chess.PlayerIndex side,
            PossibleMove movelist,
            int depth, int maxCount, int levelLimit)
        {
            // local variable
            int resultIndex = int.Parse(Thread.CurrentThread.Name);

            Board board = new Board(inboard);

            // handle my batch
            int start = interval * resultIndex;
            int end = interval * (resultIndex + 1);

            // calculate recursively
            int curCalculated = 0;
            Chess.PlayerIndex nextside = (side == Chess.PlayerIndex.Top) ? Chess.PlayerIndex.Bot : Chess.PlayerIndex.Top;

            // create the wose choice
            Choice[] nextChoices = new Choice[end - start];
            for (int i = 0; i < end - start; i++)
                nextChoices[i] = new Choice(side);


            // analyzr
            AnalyzerTreeBuilder builder = new AnalyzerTreeBuilder(depth);


            // for all remaining
            int index = start;
            for (; index < end && index < movelist.total; index++)
            {
                // move and calculated
                board.GetMoveWithChess(movelist.movements[index]);
                board.MoveBoard(movelist.movements[index]);

                // find the choice
                nextChoices[index - start] = RecurChoose(builder, movelist.movements[index], board, nextside, depth - 1, ref curCalculated, maxCount, levelLimit, true);
                nextChoices[index - start].move = movelist.movements[index];

                // revert the change it made
                board.RevertBoard(movelist.movements[index]);
            }

            // compute best choice
            results[resultIndex] = GetBestChoice(nextChoices, index - start, side);

#if isAnalyze
            // compute results
            builder.InsertNode(depth, results[resultIndex], board, null);
            analyzers[resultIndex] = (index - start == 0) ? null : builder.GetTreeRoot();
#endif
        }

        private static Choice RecurChoose(
            AnalyzerTreeBuilder builder, Move upLayerMove,
            Board board, Chess.PlayerIndex side,
            int depth, ref int calCount, int maxCount, int levelLimit,
            bool isEnemyLevel)
        {
            // calculate this node. It should take no time. doesnt matter to calculate it first
            Choice tentativechoice;

            // used one quota for entering here
            calCount++;

            if (
                // if there is no result at first glance, need to calculate next level
                board.Getwinner() == Chess.PlayerIndex.Empt && depth > 0 &&

                // if it is top layer, always proceed. If not, if it excceed, then do nothing
                (isEnemyLevel || maxCount > calCount))
            {

                // set to the worse case and try to find the best solution. 
                tentativechoice = new Choice(side);

                // for all possible next move, calculate the immediate score
                PossibleMove movelist = new PossibleMove();
                movelist.GetMoves(board, side);

                BoardVal[] scores = new BoardVal[movelist.total];
                Parallel.For(0, movelist.total, i => scores[i] = new BoardVal());

                for (int loop = 0; loop < movelist.total; loop++)
                {
                    // next board
                    board.GetMoveWithChess(movelist.movements[loop]);
                    board.MoveBoard(movelist.movements[loop]);

                    // get values
                    scores[loop].CalBoardVal(board, side);

                    // revert the change it made
                    board.RevertBoard(movelist.movements[loop]);
                }

                // sort the immediate score
                SortMoves(movelist, scores, 0, movelist.total);

                // actual recursive call
                int next = (side == Chess.PlayerIndex.Top) ? -1 : 1;
                int recurloop = (side == Chess.PlayerIndex.Top) ? movelist.total - 1 : 0;
                Chess.PlayerIndex nextside = (side == Chess.PlayerIndex.Top) ? Chess.PlayerIndex.Bot : Chess.PlayerIndex.Top;

                // undeterministic
                int mycount = 0;
                for (; recurloop >= 0 && recurloop < movelist.total; recurloop += next)
                {

                    // next board
                    board.GetMoveWithChess(movelist.movements[recurloop]);
                    board.MoveBoard(movelist.movements[recurloop]);

                    // next result
                    Choice nextchoice = RecurChoose(builder, movelist.movements[recurloop], board, nextside, depth - 1, ref calCount, maxCount, levelLimit, !isEnemyLevel);

                    // revert the change it made
                    board.RevertBoard(movelist.movements[recurloop]);

                    // compare two result
                    if (Choice.IsBetterChoice(nextchoice, tentativechoice, side) >= 0)
                    {
                        // copy choice
                        tentativechoice = nextchoice;

                        // the position is used for debug only
                        tentativechoice.move = movelist.movements[recurloop];
                    }

                    // reaches best result
                    if (tentativechoice.winner == side)
                        break;

                    // second cutting point. Will exist no matter what. If it is losing. Treat it as undeterministic
                    if (!isEnemyLevel)
                    {
                        mycount++;
                        if (mycount >= levelLimit && tentativechoice.winner == Chess.PlayerIndex.Empt)
                        {
                            break;
                        }
                    }

                    // this is level1. There is only one best choice here. 
                    if (depth == 1)
                        break;
                }
            }
            else
            {
                tentativechoice = new Choice(board, side)
                {
                    winner = board.Getwinner()
                };

            }

#if isAnalyze
            builder.InsertNode(depth, tentativechoice, board, upLayerMove);
#endif
            return tentativechoice;
        }

        private static void Swap(PossibleMove moves, BoardVal[] values, int i, int j)
        {
            // copy i to temp
            BoardVal tempvalue = values[i];
            Move tempfrom = moves.movements[i];

            // copy j to i
            values[i] = values[j];
            moves.movements[i] = moves.movements[j];

            // copy temp to j
            values[j] = tempvalue;
            moves.movements[j] = tempfrom;
        }

        private static void SortMoves(PossibleMove moves, BoardVal[] values, int start, int end)
        {
            if (start >= end) return;

            int pValue = values[start].GetScore();
            int pIndex = start;

            int i;
            int eIndex = start + 1;
            for (i = start + 1; i < end; i++)
            {
                // need exchange
                if (values[i].GetScore() < pValue)
                {
                    Swap(moves, values, i, eIndex);
                    eIndex++;
                }
            }
            // exchange eIndex with pivot
            eIndex--;

            Swap(moves, values, pIndex, eIndex);

            // recursive calls
            SortMoves(moves, values, start, eIndex);
            SortMoves(moves, values, eIndex + 1, end);
        }

    }
}
