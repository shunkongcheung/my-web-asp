﻿using MyWeb.GameChess;

namespace MyWeb.GameChess
{
    public class BoardVal
    {
        public int Topval { get; set; }
        public int Botval { get; set; }

        public BoardVal() { }

        public BoardVal(int topval, int botval)
        {
            Topval = topval;
            Botval = botval;
        }

        public BoardVal(Board board, Chess.PlayerIndex side)
        {
            CalBoardVal(board, side);
        }

        public void CalBoardVal(Board board, Chess.PlayerIndex side)
        {

            Topval = 0;
            Botval = 0;

            // top and bot val
            int i, j;
            for (i = 0; i < Board.BoardHeight; i++)
                for (j = 0; j < Board.BoardWidth; j++)
                {
                    if (board.GetChessPlayer(i, j) == Chess.PlayerIndex.Top)
                    {
                        if (board.GetChessName(i, j) == Chess.RoleIndex.GENERAL) Topval += 70;
                        if (board.GetChessName(i, j) == Chess.RoleIndex.CHARIOT) Topval += 5;
                        if (board.GetChessName(i, j) == Chess.RoleIndex.HORSE) Topval += 4;
                        if (board.GetChessName(i, j) == Chess.RoleIndex.ARTILLERY) Topval += 4;
                        if (board.GetChessName(i, j) == Chess.RoleIndex.ESCORT) Topval += 3;
                        if (board.GetChessName(i, j) == Chess.RoleIndex.JUMBO) Topval += 2;
                        if (board.GetChessName(i, j) == Chess.RoleIndex.SOLDIER) Topval += 1;
                    }
                    if (board.GetChessPlayer(i, j) == Chess.PlayerIndex.Bot)
                    {
                        if (board.GetChessName(i, j) == Chess.RoleIndex.GENERAL) Botval += 70;
                        if (board.GetChessName(i, j) == Chess.RoleIndex.CHARIOT) Botval += 5;
                        if (board.GetChessName(i, j) == Chess.RoleIndex.HORSE) Botval += 4;
                        if (board.GetChessName(i, j) == Chess.RoleIndex.ARTILLERY) Botval += 4;
                        if (board.GetChessName(i, j) == Chess.RoleIndex.ESCORT) Botval += 3;
                        if (board.GetChessName(i, j) == Chess.RoleIndex.SOLDIER) Botval += 1;
                    }
                }
        }


        public int GetScore() { return Topval - Botval; }
        

    };
}
