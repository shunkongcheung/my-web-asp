﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MyWeb.GameChess
{
    public class Move
    {
        public Coord FromCoord { get;  }
        public Coord ToCoord { get;  }

        public Chess FromChess { get; set; }
        public Chess ToChess { get; set; }

        public Move() {
            FromCoord = new Coord(-1, -1);
            ToCoord = new Coord(-1, -1);

            FromChess = new Chess();
            ToChess = new Chess();
        }

        public Move(int fromrow, int fromcol, int torow, int tocol, 
            Chess.RoleIndex fromrole, Chess.PlayerIndex fromplayer,
            Chess.RoleIndex torole, Chess.PlayerIndex toplayer)
        {
            FromCoord = new Coord(fromrow, fromcol);
            ToCoord = new Coord(torow, tocol);

            FromChess = new Chess(fromrole, fromplayer);
            ToChess = new Chess(torole, toplayer);
        }

        public Move(int fromrow, int fromcol, int torow, int tocol)
        {
            FromCoord = new Coord(fromrow, fromcol);
            ToCoord = new Coord(torow, tocol);

            FromChess = new Chess();
            ToChess = new Chess();
        }

        public Move(Move src)
        {
            FromCoord = new Coord(src.FromCoord.Row, src.FromCoord.Col);
            ToCoord = new Coord(src.ToCoord.Row, src.ToCoord.Col);

            if (src.FromChess != null)
                FromChess = new Chess(src.FromChess.Role, src.FromChess.Side);
            else
                FromChess = null;

            if (src.ToChess != null)
                ToChess = new Chess(src.ToChess.Role, src.ToChess.Side);
            else
                ToChess = null;

        }

        public void SetPosition(int fr, int fc, int tr, int tc)
        {
            FromCoord.Row = fr; FromCoord.Col = fc;
            ToCoord.Row = tr; ToCoord.Col = tc;
        }

        public static bool IsEqualCoords(Move m1, Move m2)
        {
            return Coord.IsEqual(m1.FromCoord, m2.FromCoord) &&
                    Coord.IsEqual(m1.ToCoord, m2.ToCoord);
        }
    }
}