﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MyWeb.GameChess
{
    public class Coord
    {
        public int Row { get; set; }
        public int Col { get; set; }

        public Coord(int row, int col)
        {
            Row = row;
            Col = col;
        }
        
        public static bool IsEqual(Coord c1, Coord c2)
        {
            return (c1.Row == c2.Row && c1.Col == c2.Col);
        }
    }
}