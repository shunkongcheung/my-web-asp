﻿
using MyWeb.GameChess;

namespace MyWeb.GameChess
{
    // helper structure for finding best route
    public class Choice
    {
        // move
        public Move move;
        
        // result
        public Chess.PlayerIndex winner;
        public BoardVal boardValue;
        
                
        public Choice(Board board, Chess.PlayerIndex side)
        {
            move = new Move(-1,-1,-1,-1);
            
            winner = Chess.PlayerIndex.Empt;
            boardValue = new BoardVal(board, side);

            //isAllCalculated = false;
        }

        public Choice( Choice src)
        {
            move = (src.move == null)? null: new Move(src.move);
            winner = src.winner;
            boardValue = new BoardVal(src.boardValue.Topval, src.boardValue.Botval);

        }

        public Choice (Chess.PlayerIndex side)
        {
            // st to worse case
            winner = (side == Chess.PlayerIndex.Top) ? Chess.PlayerIndex.Bot : Chess.PlayerIndex.Top;
            boardValue = new BoardVal(((side == Chess.PlayerIndex.Top) ? 0 : 500), ((side == Chess.PlayerIndex.Bot) ? 0 : 500));
        }



        // helper function for comparing Choice
        // return true if c1 is better than c2
        public static int IsBetterChoice(Choice c1, Choice c2, Chess.PlayerIndex side)
        {
            // no difference in winner
            if (c1.winner == c2.winner)
            {
                // no difference in board value
                int c1Value = c1.boardValue.GetScore();
                int c2Value = c2.boardValue.GetScore();

                // equal. not better
                if (c1Value == c2Value)
                {
                    // the one with higher total value is better
                    // more reserved power
                    int c1Total = c1.boardValue.Topval + c1.boardValue.Botval;
                    int c2Total = c2.boardValue.Topval + c2.boardValue.Botval;

                    if (c1Total > c2Total) return 1;
                    else if (c1Total < c2Total) return -1;

                    // exactly same value
                    return 0;
                }


                // if it is a top player
                else if (side == Chess.PlayerIndex.Top)
                {
                    // value is better
                    if (c1Value > c2Value) return 1;
                    // value is worse
                    else if (c1Value < c2Value) return -1;
                }

                // if it is a bottom player
                else
                {
                    // value is better
                    if (c1Value < c2Value) return 1;
                    // value is worse
                    else if (c1Value > c2Value) return -1;
                }
            }
            // has difference in winner and c1 is winner
            else if (c1.winner == side) { return 1; }

            // c1 is empty, depends on c2 then
            else if (c1.winner == Chess.PlayerIndex.Empt)
            {
                if (c2.winner == side) return -1;
                else return 1;
            }
            // c1 is losing, and c2 is different, then c1 must be worse than c2
            return -1;
        }
    };
}
