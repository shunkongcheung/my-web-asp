using Microsoft.VisualStudio.TestTools.UnitTesting;
using MyWeb.GameChess;
using MyWeb.WgStatic;

namespace UnitTestDataBase
{
    [TestClass]
    public class UnitTestCache
    {
        
        [TestMethod]
        public void TestInsertAndRetrieve()
        {
            MyCache cache = new MyCache(10);

            string id = "id";
            string value = "value";
            cache.InsertItem(id, value);

            string retrieve = (string)cache.GetItem(id);

            Assert.AreEqual(value, retrieve);
        }
        
        [TestMethod]
        public void TestGetUnExist()
        {
            MyCache cache = new MyCache(10);

            string id = "id";
            string retrieve = (string)cache.GetItem(id);
            Assert.IsNull(retrieve);
        }

        [TestMethod]
        public void TestStoreDifferentType()
        {
            MyCache cache = new MyCache(10);

            string idstr = "id";
            string valuestr = "value";

            string idchess = "chess";
            Chess valuechess = new Chess();

            cache.InsertItem(idstr, valuestr);
            string retrievestr = (string)cache.GetItem(idstr);

            cache.InsertItem(idchess, valuechess);
            Chess retrievechess = (Chess)cache.GetItem(idchess);

            Assert.AreEqual(valuestr, retrievestr);
            Assert.AreEqual(valuechess, retrievechess);
        }

        [TestMethod]
        public void TestStoreSameKey()
        {
            // set the size to 2, storing 2 same key should only save 1.
            // storing 3rd item should be okay and should be able to get it back
            MyCache cache = new MyCache(2);
            
            // check if it is stored
            cache.InsertItem("id", "str1");
            cache.InsertItem("id", "str2");
            Assert.AreEqual("str2", (string)cache.GetItem("id"));
            
            Chess valuechess = new Chess();
            cache.InsertItem("chess", valuechess);

            Assert.IsNotNull(cache.GetItem("chess"));
            Assert.AreEqual(valuechess, cache.GetItem("chess"));
        }

        [TestMethod]
        public void TestStoreOverLimit()
        {
            MyCache cache = new MyCache(1);
            cache.InsertItem("2", "2");
            cache.InsertItem("1", "1");

            Assert.AreEqual("1", (string)cache.GetItem("1") );
            Assert.IsNull(cache.GetItem("2"));
        }
    }

}
