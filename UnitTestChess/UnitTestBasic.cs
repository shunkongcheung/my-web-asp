﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using MyWeb.GameChess;

namespace UnitTestChess
{
    [TestClass]
    public class UnitTestChess
    {
        [TestMethod]
        public void TestChessEqual()
        {
            Chess c1 = new Chess(Chess.RoleIndex.ARTILLERY, Chess.PlayerIndex.Bot);
            bool isEuqal = Chess.IsEqual(c1, c1);
            Assert.IsTrue(isEuqal);
        }

        [TestMethod]
        public void TestChessRoleNotEqual()
        {
            Chess c1 = new Chess(Chess.RoleIndex.ARTILLERY, Chess.PlayerIndex.Bot);
            Chess c2 = new Chess(Chess.RoleIndex.CHARIOT, Chess.PlayerIndex.Bot);
            bool isEuqal = Chess.IsEqual(c1, c2);
            Assert.IsFalse(isEuqal);
        }

        [TestMethod]
        public void TestChessSideNotEqual()
        {
            Chess c1 = new Chess(Chess.RoleIndex.ARTILLERY, Chess.PlayerIndex.Bot);
            Chess c2 = new Chess(Chess.RoleIndex.ARTILLERY, Chess.PlayerIndex.Top);
            bool isEuqal = Chess.IsEqual(c1, c2);
            Assert.IsFalse(isEuqal);
        }
    }

}
