﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using MyWeb.GameChess;
using System;
using System.Collections.Generic;
using System.IO;
using System.Xml;

namespace UnitTestChess
{
    [TestClass]
    public class UnitTestPossibleMove
    {
        [TestMethod]
        public void TestPossibleMoveChaseBoard()
        {
            TestBoard("../../../testboard/chasing.xml");
        }

        [TestMethod]
        public void TestPossibleMoveDeadBoard()
        {
            TestBoard("../../../testboard/dead.xml");
        }

        [TestMethod]
        public void TestPossibleMoveMiddleBoard()
        {
            TestBoard("../../../testboard/middle.xml");
        }
        [TestMethod]
        public void TestPossibleMoveUpToTurnBoard()
        {
            TestBoard("../../../testboard/uptoturn.xml");
        }


        private void TestBoard(String path)
        {
            String filecontent = File.ReadAllText(path);
            Board expectedboard = new Board(filecontent);

            PossibleMove movement = new PossibleMove();

            List<Move> expectToplist = GetCoordFromFile(filecontent, Chess.PlayerIndex.Top);
            movement.GetMoves(expectedboard, Chess.PlayerIndex.Top);
            Assert.IsTrue(IsAllExist(expectToplist, movement));

            List<Move> expectBotlist = GetCoordFromFile(filecontent, Chess.PlayerIndex.Bot);
            movement.GetMoves(expectedboard, Chess.PlayerIndex.Bot);
            Assert.IsTrue(IsAllExist(expectBotlist, movement));
        }

        private bool IsAllExist(List<Move> expectList, PossibleMove movement)
        {

            foreach (Move expect in expectList)
            {
                // look for it in movement
                bool exist = false;
                for (int i = 0; i < movement.total && !exist; i++)
                    exist = Move.IsEqualCoords(expect, movement.movements[i]);

                // if does not exist
                if (!exist)
                {
                    Console.Error.WriteLine("This expect move does not exist: " 
                        + "(" + expect.FromCoord.Row + ", " + expect.FromCoord.Col + ")"
                        + "(" + expect.ToCoord.Row + ", " + expect.ToCoord.Col + ")"
                        );
                    return false;
                }
            }


            return true;
        }

        private List<Move> GetCoordFromFile(String filecontent, Chess.PlayerIndex side)
        {
            List<Move> list = new List<Move>();

            XmlDocument xmlBoard = new XmlDocument();
            xmlBoard.LoadXml(filecontent);
            XmlNodeList moves = xmlBoard.SelectNodes("board/" + ((side == Chess.PlayerIndex.Top) ? "t" : "b"));

            foreach (XmlNode move in moves)
            {
                int fr = int.Parse(move.Attributes["fr"].Value);
                int fc = int.Parse(move.Attributes["fc"].Value);
                int tr = int.Parse(move.Attributes["tr"].Value);
                int tc = int.Parse(move.Attributes["tc"].Value);
                list.Add(new Move(fr, fc, tr, tc));
            }

            return list;
        }


    }
}
