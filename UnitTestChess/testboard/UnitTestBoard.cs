﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using MyWeb.GameChess;
using System;
using System.IO;

namespace UnitTestChess.testboard
{
    [TestClass] 
    public class UnitTestBoard
    {
        #region build
        [TestMethod]
        public void TestBoardBuildEmptyBoard()
        {
            Board board = new Board();

            for (int i = 0; i < Board.BoardHeight; i++)
                for (int j = 0; j < Board.BoardWidth; j++)
                {
                    Assert.AreEqual(Chess.RoleIndex.NOTHING, board.GetChessName(i, j));
                    Assert.AreEqual(Chess.PlayerIndex.Empt, board.GetChessPlayer(i, j));
                }
        }

        [TestMethod]
        public void TestBoardCopyBoard()
        {
            String filecontent = File.ReadAllText("../../../testboard/chasing.xml");
            Board board = new Board(filecontent);
            Board copy = new Board(board);

            for (int i = 0; i < Board.BoardHeight; i++)
                for (int j = 0; j < Board.BoardWidth; j++)
                {
                    Assert.AreEqual(board.GetChessName(i, j), copy.GetChessName(i, j));
                    Assert.AreEqual(board.GetChessPlayer(i, j), copy.GetChessPlayer(i, j));
                }
        }
        #endregion

        #region winner
        [TestMethod]
        public void TestBoardGetWinnerOnDead()
        {
            String filecontent = File.ReadAllText("../../../testboard/dead.xml");
            Board board = new Board(filecontent);

            Assert.AreEqual(Chess.PlayerIndex.Top, board.Getwinner());
        }

        [TestMethod]
        public void TestBoardGetWinnerOnChase()
        {
            String filecontent = File.ReadAllText("../../../testboard/chasing.xml");
            Board board = new Board(filecontent);

            Assert.AreEqual(Chess.PlayerIndex.Empt, board.Getwinner());
        }
        #endregion

        #region move&revert 
        [TestMethod]
        public void TestBoardGetFullMove()
        {
            String filecontent = File.ReadAllText("../../../testboard/chasing.xml");
            Board branchmarkBoard = new Board(filecontent);
            Board board = new Board(filecontent);

            Move move = new Move(0, 4, 0, 3);
            board.GetMoveWithChess(move);

            Assert.AreEqual(branchmarkBoard.GetChessName(move.FromCoord.Row, move.FromCoord.Col), move.FromChess.Role);
            Assert.AreEqual(branchmarkBoard.GetChessPlayer(move.FromCoord.Row, move.FromCoord.Col), move.FromChess.Side);
            

            Assert.AreEqual(branchmarkBoard.GetChessName(move.ToCoord.Row, move.ToCoord.Col), move.ToChess.Role);
            Assert.AreEqual(branchmarkBoard.GetChessPlayer(move.ToCoord.Row, move.ToCoord.Col), move.ToChess.Side);
        }

        [TestMethod]
        public void TestBoardMove()
        {
            String filecontent = File.ReadAllText("../../../testboard/chasing.xml");
            Board board = new Board(filecontent);
            
            Move move = new Move(0, 4, 0, 3);
            Chess branchChess = new Chess(board.GetChessName(move.FromCoord.Row, move.FromCoord.Col), board.GetChessPlayer(move.FromCoord.Row, move.FromCoord.Col));
            board.MoveBoard(move);

            Assert.AreEqual(Chess.PlayerIndex.Empt, board.GetChessPlayer(move.FromCoord.Row, move.FromCoord.Col));
            Assert.AreEqual(Chess.RoleIndex.NOTHING, board.GetChessName(move.FromCoord.Row, move.FromCoord.Col));
            
            Assert.AreEqual(branchChess.Side, board.GetChessPlayer(move.ToCoord.Row, move.ToCoord.Col));
            Assert.AreEqual(branchChess.Role, board.GetChessName(move.ToCoord.Row, move.ToCoord.Col));
        }

        [TestMethod]
        public void TestBoardRevert()
        {
            String filecontent = File.ReadAllText("../../../testboard/chasing.xml");
            Board board = new Board(filecontent);

            Move move = new Move(0, 4, 0, 3, Chess.RoleIndex.HORSE, Chess.PlayerIndex.Bot, Chess.RoleIndex.ARTILLERY, Chess.PlayerIndex.Top);
            board.RevertBoard(move);

            Assert.AreEqual(move.FromChess.Side, board.GetChessPlayer(move.FromCoord.Row, move.FromCoord.Col));
            Assert.AreEqual(move.FromChess.Role, board.GetChessName(move.FromCoord.Row, move.FromCoord.Col));

            Assert.AreEqual(move.ToChess.Side, board.GetChessPlayer(move.ToCoord.Row, move.ToCoord.Col));
            Assert.AreEqual(move.ToChess.Role, board.GetChessName(move.ToCoord.Row, move.ToCoord.Col));
        }

        #endregion
    }
}
