using Microsoft.VisualStudio.TestTools.UnitTesting;
using MyWeb.GameChess;
using System;
using System.IO;

namespace UnitTestChess
{
    [TestClass]
    public class UnitTestChoose
    {
        private static Choose choose;
        
        [ClassInitialize()]
        public static void UnitTestChooseInit(TestContext context)
        {
            // Initalization code goes here
            choose = new Choose(Choose.Difficulty.Difficult);
        }

        [TestMethod]
        public void TestchooseOnChaseBoard()
        {
            String filecontent = File.ReadAllText("../../../testboard/chasing.xml");
            Board expectedboard = new Board(filecontent);
            Choice chaset = choose.MakeChoice(expectedboard, Chess.PlayerIndex.Top);
            Choice chaseb = choose.MakeChoice(expectedboard, Chess.PlayerIndex.Bot);

            Assert.AreEqual(Chess.PlayerIndex.Top, chaset.winner);
            Assert.AreEqual(Chess.PlayerIndex.Empt, chaseb.winner);
        }

        [TestMethod]
        public void TestchooseOnDeadBoard()
        {
            String filecontent = File.ReadAllText("../../../testboard/dead.xml");
            Board expectedboard = new Board(filecontent);
            Choice chaset = choose.MakeChoice(expectedboard, Chess.PlayerIndex.Top);
            Choice chaseb = choose.MakeChoice(expectedboard, Chess.PlayerIndex.Bot);

            Assert.AreEqual(Chess.PlayerIndex.Top, chaset.winner);
            Assert.AreEqual(Chess.PlayerIndex.Top, chaseb.winner);
        }

        [TestMethod]
        public void TestchooseOnMiddleBoard()
        {
            String filecontent = File.ReadAllText("../../../testboard/middle.xml");
            Board expectedboard = new Board(filecontent);
            Choice chaset = choose.MakeChoice(expectedboard, Chess.PlayerIndex.Top);
            Choice chaseb = choose.MakeChoice(expectedboard, Chess.PlayerIndex.Bot);

            Assert.AreEqual(Chess.PlayerIndex.Empt, chaset.winner);
            Assert.AreEqual(Chess.PlayerIndex.Empt, chaseb.winner);
        }

        [TestMethod]
        public void TestchooseUpToTurnBoard()
        {
            String filecontent = File.ReadAllText("../../../testboard/uptoturn.xml");
            Board expectedboard = new Board(filecontent);
            Choice chaset = choose.MakeChoice(expectedboard, Chess.PlayerIndex.Top);
            Choice chaseb = choose.MakeChoice(expectedboard, Chess.PlayerIndex.Bot);

            Assert.AreEqual(Chess.PlayerIndex.Top, chaset.winner);
            Assert.AreEqual(Chess.PlayerIndex.Bot, chaseb.winner);
        }


        #region OutputMethod
        //private void OutputString(String message)
        //{
        //    Console.WriteLine(message);
        //}

        //private void OutputBoard(Board board)
        //{
        //    StringWriter strWriter = new StringWriter();
        //    strWriter.WriteLine("------------------------");

        //    for (int i = 0; i < Board.BoardHeight; i++)
        //    {
        //        for (int j = 0; j < Board.BoardWidth; j++)
        //        {
        //            if (board.GetChessPlayer(i, j) == Chess.Player.Top) strWriter.Write("{0}", (char)board.GetChessName(i, j));
        //            if (board.GetChessPlayer(i, j) == Chess.Player.Bot) strWriter.Write("{0}", (char)(board.GetChessName(i, j) - 'A' + 'a'));
        //            if (board.GetChessPlayer(i, j) == Chess.Player.Empt) strWriter.Write(".");
        //        }
        //        strWriter.WriteLine();
        //    }

        //    OutputString(strWriter.ToString());
        //    OutputString("------------------------");
        //}

        //private void OutputChoice(Choice choice, Board board)
        //{
        //    // move
        //    StringWriter strwriter = new StringWriter();
        //    strwriter.Write("Choice: ({0}, {1}: {2})-> ({3},{4}: {5})",
        //        choice.fromrow, choice.fromcol, board.GetChessName(choice.fromrow, choice.fromcol),
        //        choice.torow, choice.tocol, board.GetChessName(choice.torow, choice.tocol)
        //        );
        //    OutputString(strwriter.ToString());

        //    // expectation
        //    strwriter = new StringWriter();
        //    strwriter.Write("Expectation: [{0}]. ({1}, {2}).",
        //        choice.winner,
        //        choice.boardValue.GetTopVal(), choice.boardValue.GetBotVal()
        //        );

        //    OutputString(strwriter.ToString());

        //    // board
        //    OutputBoard(board);
        //}
        #endregion
    }

}
