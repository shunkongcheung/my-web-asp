﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using MyWeb.GameChess;

namespace UnitTestChess
{
    [TestClass]
    public class UnitTestMove
    {
        [TestMethod]
        public void TestMoveEqualCoord()
        {
            Move move = new Move(1,2, 3,4, Chess.RoleIndex.ARTILLERY, Chess.PlayerIndex.Top, Chess.RoleIndex.NOTHING, Chess.PlayerIndex.Empt);
            Assert.IsTrue(Move.IsEqualCoords(move, move));
        }

        [TestMethod]
        public void TestMoveEqualCoordWithDifferentRole()
        {
            Move move1 = new Move(1, 2, 3, 4, Chess.RoleIndex.ARTILLERY, Chess.PlayerIndex.Top, Chess.RoleIndex.NOTHING, Chess.PlayerIndex.Empt);
            Move move2 = new Move(1, 2, 3, 4, Chess.RoleIndex.SOLDIER, Chess.PlayerIndex.Top, Chess.RoleIndex.NOTHING, Chess.PlayerIndex.Empt);
            Assert.IsTrue(Move.IsEqualCoords(move1, move2));

        }

        [TestMethod]
        public void TestMoveEqualCoordWithDifferentSide()
        {
            Move move1 = new Move(1, 2, 3, 4, Chess.RoleIndex.ARTILLERY, Chess.PlayerIndex.Top, Chess.RoleIndex.NOTHING, Chess.PlayerIndex.Empt);
            Move move2 = new Move(1, 2, 3, 4, Chess.RoleIndex.ARTILLERY, Chess.PlayerIndex.Bot, Chess.RoleIndex.NOTHING, Chess.PlayerIndex.Empt);
            Assert.IsTrue(Move.IsEqualCoords(move1, move2));
        }

        [TestMethod]
        public void TestMoveNotEqualCoord()
        {
            Move move1 = new Move(1, 2, 3, 4);
            Move move2 = new Move(5, 2, 3, 4);
            Assert.IsFalse(Move.IsEqualCoords(move1, move2));

             move1 = new Move(1, 2, 3, 4);
             move2 = new Move(1, 5, 3, 4);
            Assert.IsFalse(Move.IsEqualCoords(move1, move2));

             move1 = new Move(1, 2, 3, 4);
             move2 = new Move(1, 2, 5, 4);
            Assert.IsFalse(Move.IsEqualCoords(move1, move2));

             move1 = new Move(1, 2, 3, 4);
             move2 = new Move(1, 2, 3, 5);
            Assert.IsFalse(Move.IsEqualCoords(move1, move2));
            
        }
    }
}
